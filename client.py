#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
ERROR = "client.py ip puerto register sip_address expires_value"

# Control de argumentos
if len(sys.argv) == 6:
    try:
        SERVER = sys.argv[1]
        PORT = int(sys.argv[2])
        PETICION = str.upper(sys.argv[3])
        USUARIO = sys.argv[4]
        EXPIRES = int(sys.argv[5])
        REGISTER_SIP = PETICION + " sip:" + USUARIO + " SIP/2.0\r\n"
        EXPIRES_SIP = "Expires: " + str(EXPIRES) + "\r\n\r\n"
    except:
        sys.exit("Usage: " + ERROR)
else:
    sys.exit("Usage: " + ERROR)

# Generamos la peticion
if PETICION == "REGISTER":
    LINE = REGISTER_SIP + EXPIRES_SIP
else:
    sys.exit("Needed: " + ERROR)

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print(data.decode('utf-8'))

print("Socket terminado.")
