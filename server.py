#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
# Paquetes utilizados
import json
import sys
import socketserver
from datetime import datetime, date, time, timedelta

# Control de argumentos
if len(sys.argv) == 2:
    try:
        Port_Server = int(sys.argv[1])
        FORMATO = "%Y-%m-%d %H:%M:%S"
    except:
        sys.exit("Needed: python3 server.py <port>")
else:
    sys.exit("Needed: python3 server.py <port>")


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    # SIP Register Handler
    dicc = {}

    def json2register(self):
        """
        En caso de que el fichero exista cargamos y guardamos
        su contenido.
        """
        try:
            with open("registered.json", "r") as jsonfile:
                self.dicc = json.load(jsonfile)
        except:
            """
            En caso de que no exista, se mantiene.
            """
            pass

    def register2json(self):
        """
        Escribimos el diccionario en el fichero registered.json
        """
        with open("registered.json", "w") as jsonfile:
            json.dump(self.dicc, jsonfile, indent=3)

    def handle(self):
        """
        Se crea el diccionario a partir del registered.json
        """
        self.json2register()
        usuario_dicc = {"address": "", "expires": ""}
        IP_Client = self.client_address[0]
        PORT_Client = self.client_address[1]
        list = []

        for line in self.rfile:
            list.append(line.decode("utf-8"))

        mensaje = list[0].split(" ")

        # Comprobamos que la peticion es REGISTER
        if mensaje[0] == "REGISTER":
            usuario = mensaje[1].split(":")[1]
            mensaje = list[1].split()
            expires = mensaje[1].split("\r\n")[0]

            expires_date = datetime.now() + timedelta(seconds=int(expires))
            usuario_dicc["address"] = IP_Client
            usuario_dicc["expires"] = expires_date.strftime(FORMATO)

            # Si el expires es 0, se borra al cliente del diccionario
            if int(expires) == 0:
                try:
                    del self.dicc[usuario]
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                except KeyError:
                    pass

            # Si el expires es distinto de 0, se guarda al usuario en el dicc
            else:
                self.dicc[usuario] = usuario_dicc
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

            """
            Comprobamos si algun cliente tiene la sesion caducada y
            en tal caso se borra del dicc, y se modifica el fichero.
            """

            hora_actual = datetime.now().strftime(FORMATO)
            usuario_borrado = []

            for usuario in self.dicc:
                if hora_actual >= self.dicc[usuario]['expires']:
                    usuario_borrado.append(usuario)
            for usuario in usuario_borrado:
                del self.dicc[usuario]

            self.register2json()


if __name__ == "__main__":
    """
    Se crea el servicio UDP en el puerto indicado
    """
    serv = socketserver.UDPServer(('', Port_Server), SIPRegisterHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        # Creamos el servidor
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
